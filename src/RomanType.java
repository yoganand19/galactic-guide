import java.util.HashMap;
import java.util.Map;

/*
 * @author Yoganand Dharmaraj
 * @description
 * RomanType is used for 
 * 1. Validating a given roman value
 * 2. Converting a roman value to numeric value 
 */

public class RomanType {
	
	// Map of roman character and its numeric value
	private static final Map<Character, Integer> map_RomanNumeralToNumericValue = new
		HashMap<Character, Integer>() {
			{
				put('I', 1);
				put('V', 5);
				put('X', 10);
				put('L', 50);
				put('C', 100);
				put('D', 500);
				put('M', 1000);
			}
		};
	
	// Map of roman character to roman character(s) from which the key is subtractable
	private static final Map<Character, Character[]> map_SubtractableRomanChars = new
		HashMap<Character, Character[]>() {
			{
				put('I', new Character[] {'V', 'X'});
				put('V', new Character[] {});
				put('X', new Character[] {'L', 'C'});
				put('L', new Character[] {});
				put('C', new Character[] {'D', 'M'});
				put('D', new Character[] {});
				put('M', new Character[] {});
			}
		};

	// Map to keep track of count of both repeatable/non-repeatable characters in a given roman string
	private static Map<Character, Integer> map_RepeatableChars = resetRepeatableCharsCount();
	private static Map<Character, Integer> map_NonRepeatableChars = resetNonRepeatableCharsCount();

	private static Map<Character, Integer> resetRepeatableCharsCount() {
		Map<Character, Integer> m = new HashMap<Character,Integer>() {
			{
				put('I', 0);
				put('X', 0);
				put('C', 0);
				put('M', 0);
			}
		};
		return m;
	}
	
	private static Map<Character, Integer> resetNonRepeatableCharsCount() {
		Map<Character, Integer> m = new HashMap<Character,Integer>() {
			{
				put('V', 0);
				put('L', 0);
				put('D', 0);
			}
		};
		return m;
	}
	
	// To reset the count values of repeatable/non-repeatable characters
	private static void resetCharacterCounts() {
		map_RepeatableChars = resetRepeatableCharsCount();
		map_NonRepeatableChars = resetNonRepeatableCharsCount();
	}

	// Convert a given roman string to its numeric equivalent
	public static Integer convertToNumeric(String romanVal) {
		
		Integer value = 0;

		if(!isValid(romanVal))
			return -1;

		char[] romanCharArray = romanVal.toUpperCase().toCharArray();
		Character currChar, nextChar;
		Integer currVal, nextVal;
		currVal = 0;
		nextVal = 0;
		
		// Keep adding the values from left to right
		// Only exception is when the next character is of higher value, perform subtraction 
		// and add it to the final value.
		for(int i=0; i<romanCharArray.length; ) {
			currChar = romanCharArray[i];
			nextChar = (i+1) < romanCharArray.length ? romanCharArray[i+1] : null;

			currVal = map_RomanNumeralToNumericValue.get(currChar);
			nextVal = 0;
			if(nextChar != null)
				nextVal = map_RomanNumeralToNumericValue.get(nextChar);

			if(nextVal > currVal) {
				value += (nextVal - currVal);
				i += 2;
			} else {
				value += currVal;
				i++;
			}
		}

		return value;
	}

	// Check if the roman string is a valid one or not
	// Check for 3 conditions - valid characters, follows repeatable/non-repeatable logic, 
	// follows subtractable logic
	public static Boolean isValid(String romanVal) {
		Boolean isValidRomanFlag = true;
		resetCharacterCounts();

		char[] romanCharArray = romanVal.toUpperCase().toCharArray();
		Character currChar, nextChar;

		//System.out.println("**** : " + romanVal);
		for(int i=0; i<romanCharArray.length; i++) {

			currChar = romanCharArray[i];
			nextChar = (i+1) < romanCharArray.length ? romanCharArray[i+1] : null;

			// Check if all the characters are valid roman numerals
			if(!isValidRomanChar(currChar)) {
				isValidRomanFlag = false;
				break;
			}

			// Check for the count of repeatable and non-repeatable characters
			if(isANonRepeatableRomanChar(currChar)) {
				map_NonRepeatableChars.put(currChar, map_NonRepeatableChars.get(currChar) + 1);
				// If a non-repeatable character occurs twice, mark as invalid
				if(map_NonRepeatableChars.get(currChar) == 2) {
					isValidRomanFlag = false;
					break;
				}
			} else {
				map_RepeatableChars.put(currChar, map_RepeatableChars.get(currChar) + 1);
				// If a repeatable character appears more than 3 times, check if the 
				// preceding character is a different character with lesser value 
				// and follows subtraction logic
				if(map_RepeatableChars.get(currChar) == 4) {
					if(!checkIfSubtractable(romanCharArray[i-1], currChar) || romanCharArray[i-1] == currChar) {
						isValidRomanFlag = false;
						break;
					}
				} else if(map_RepeatableChars.get(currChar) > 4) {
					isValidRomanFlag = false;
					break;
				}
			}

			// Check if the next char (if has higher value) follows subtraction rule
			if(nextChar != null && isValidRomanChar(nextChar)) {
				if(!checkIfSubtractable(currChar, nextChar)) {
					isValidRomanFlag = false;
					break;
				}
			}
		}

		return isValidRomanFlag;
	}
	
	// Check if the roman character is a valid one
	private static Boolean isValidRomanChar(Character currChar) {
		Boolean result = false;

		if(map_RomanNumeralToNumericValue.containsKey(currChar))
			result = true;

		return result;
	}

	// Check if the roman character is non-repeatable
	private static Boolean isANonRepeatableRomanChar(Character currChar) {
		Boolean result = false;

		if(map_NonRepeatableChars.containsKey(currChar))
			result = true;

		return result;
	}
	
	// Compare if the roman character on left is subtractable from the right one
	private static Boolean checkIfSubtractable(Character currChar, Character nextChar) {
		Boolean result = false;
		
		// Only if the left roman character value is lesser, do the comparison. Otherwise it is fine
		if(map_RomanNumeralToNumericValue.get(currChar) < map_RomanNumeralToNumericValue.get(nextChar)) {
			for(Character c : map_SubtractableRomanChars.get(currChar)) {
				if(c.equals(nextChar)) {
					result = true;
					break;
				}
			}
		} else {
			result = true;
		}
		
		return result;
	}

	/*public static void main(String[] args) {
		System.out.println("Roman value of X " + RomanType.convertToNumeric("VIII"));
	}*/

}