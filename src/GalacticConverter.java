import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * @author Yoganand Dharmaraj
 * @description
 * GalacticConverter processes string with intergalactic notations
 * 1. Stores the intergalactic to roman mapping
 * 2. Stores the metal credit information
 * 3. Process questions and evaluates answers based on the above (1 and 2) 
 */

public class GalacticConverter {
	
	private Map<String, String> map_GalacticToRoman; // eg. <"glob" , "I">
	private Map<String, Float> map_MetalToUnitCredit; // eg. <"silver", 72>
	private ArrayList<String> answers;
	
	// Invalid messages
	// private static final String INVALID_QUERY = "I have no idea what you are talking about";
	private static final String INVALID_SEMANTICS = "This does not follow the expected syntax! Refer *Assumptions and Design.txt* for input syntax.";
	private static final String INVALID_ROMAN_EQUIVALENT = "The roman value is incorrect!";
	
	public GalacticConverter() {
		map_GalacticToRoman = new HashMap<String, String>();
		map_MetalToUnitCredit = new HashMap<String, Float>();
		answers = new ArrayList<String>();
	}
	
	// Handler method to decide on how an input string needs to be processed
	public void processLine(String inpLine) {
		String tokens[] = inpLine.split(" ");
		
		if(tokens.length == 3 && tokens[1].equalsIgnoreCase("is") && tokens[2].length() == 1) {
			processGalacticToRomanMapping(inpLine);
		} else if(inpLine.toLowerCase().endsWith("credits")) {
			processMetalCredits(inpLine);
		} else if(inpLine.endsWith("?")) {
			processQuestions(inpLine);
		}
		
	}
	
	// Process strings which provide the mapping from galactic notation to roman numeral
	// eg. 'glob is I'
	// Populate the map map_GalacticToRoman
	private void processGalacticToRomanMapping(String inpLine) {
		String tokens[] = inpLine.split(" ");
		String romanChar = tokens[2].trim().toUpperCase();
		String galacticStr = tokens[0].trim().toLowerCase();
		
		if(RomanType.isValid(romanChar)) {
			map_GalacticToRoman.put(galacticStr, romanChar);
		} else {
			processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_ROMAN_EQUIVALENT);
		}
	}
	
	// Process strings which specify the credits of a metal
	// eg. 'glob prok Gold is 57800 Credits'
	private void processMetalCredits(String inpLine) {
		String tokens[] = inpLine.split(" ");
		
		// There should at least be 5 tokens for specifying the credits as per given syntax
		if(tokens.length < 5) {
			processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_SEMANTICS);
			return;
		}
		
		/*
		Extract all tokens except last 4
		Build the roman string, call convertToRoman
		Find the numeric value, call roman.convertToNumeric
		If numeric value is -1, ignore. Else proceed.
		From the credit value calculate the unit price and populate map_MetalToUnitPrice
		*/
		
		String metalName = tokens[tokens.length - 4].trim();
		String token_Is = tokens[tokens.length-3].trim();
		String token_CreditValue = tokens[tokens.length-2].trim();
		
		StringBuilder galacticValue = new StringBuilder();
		String romanStr = "";
		Integer numericVal;
		Float creditsValue;
		
		if(token_Is.equalsIgnoreCase("is") && isNumeric(token_CreditValue)) {
			for(int i=0; i < tokens.length-4; i++) {
				galacticValue.append(tokens[i].trim() + " ");
			}
			romanStr = convertToRoman(galacticValue.toString().trim());
			if((numericVal = RomanType.convertToNumeric(romanStr)) != -1) {
				creditsValue = Float.parseFloat(token_CreditValue);
				creditsValue = creditsValue/numericVal;
				map_MetalToUnitCredit.put(metalName.toLowerCase(), creditsValue);
			} else {
				processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_ROMAN_EQUIVALENT);
			}
		} else {
			processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_SEMANTICS);
		}
	}
	
	// Process query strings and evaluate the answer
	// eg. 'how much is pish tegj glob glob ?'
	// eg. 'how many Credits is glob prok Silver ?'
	private void processQuestions(String inpLine) {
		
		/*
		If first 3 tokens are 'how much is'
			Extract all tokens till '?'
			Build the roman string, call convertToRoman
			Find the numeric value, call roman.convertToNumeric
			If numeric value is -1, call processInvalidQuery()
			Else add to answer[]
		Else if first 4 tokens are 'how many credits is'
			Extract the token before '?' and determine the metal
			Extract remaining tokens, call convertToRoman
			Find the numeric value, call roman.convertToNumeric and find the credits value
			If numeric value is -1, call processInvalidQuery()
			Else add to answer[]
		*/
		
		String str_Howmuchis = "how much is";
		String str_Howmanycreditsis = "how many credits is";
		
		if(inpLine.toLowerCase().startsWith(str_Howmuchis)) {
			evaluateGalacticValue(inpLine);
		} else if(inpLine.toLowerCase().startsWith(str_Howmanycreditsis)) {
			evaluateMetalCredits(inpLine);
		} else {
			processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_SEMANTICS);
		}
	}
	
	// Processes questions of the form 'how much is pish tegj glob glob ?'
	private void evaluateGalacticValue(String inpLine) {
		String tokens[] = inpLine.split(" ");
		
		StringBuilder galacticStr = new StringBuilder();
		String romanStr = "";
		Integer numericVal;
		String answer = "";
		
		// Retrieve tokens after "how much is" till "?"
		for(int i=3; i < tokens.length-1; i++) {
			galacticStr.append(tokens[i].trim() + " ");
		}
		answer += galacticStr + "is ";
		romanStr = convertToRoman(galacticStr.toString());
		if((numericVal = RomanType.convertToNumeric(romanStr)) != -1) {
			answer += numericVal.toString();
			processAnswer(answer);
		} else {
			processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_ROMAN_EQUIVALENT);
		}
	}
	
	// Process questions of the form 'how many Credits is glob prok Silver ?'
	private void evaluateMetalCredits(String inpLine) {
		String tokens[] = inpLine.split(" ");
		
		StringBuilder galacticStr = new StringBuilder();
		String romanStr = "";
		Integer numericVal;
		Float unitCreditValue, totalCreditsValue;
		String answer = "";
		String metalName = tokens[tokens.length-2];
		
		if(map_MetalToUnitCredit.containsKey(metalName.toLowerCase())) {
			
			// Retrieve tokens after "how many credits is" till "?"
			for(int i=4; i < tokens.length-2; i++) {
				galacticStr.append(tokens[i] + " ");
			}
			
			// Find the numeric value and build the answer
			answer += galacticStr + "is ";
			romanStr = convertToRoman(galacticStr.toString());
			if((numericVal = RomanType.convertToNumeric(romanStr)) != -1) {
				unitCreditValue = map_MetalToUnitCredit.get(metalName.toLowerCase());
				totalCreditsValue = unitCreditValue * numericVal;
				answer += totalCreditsValue.toString() + " Credits";
				processAnswer(answer);
			} else {
				processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_ROMAN_EQUIVALENT);
			}
		} else {
			processInvalidQuery(inpLine + " : " + GalacticConverter.INVALID_SEMANTICS);
		}
	}
	
	// Placeholder method - to process invalid statements and queries
	private void processInvalidQuery(String errorStr) {
		answers.add(errorStr);
	}
	
	// Placeholder method - to process the answers to queries
	private void processAnswer(String answerStr) {
		//System.out.println("Answer called");
		answers.add(answerStr);
	}
	
	// To print all the answers
	public void showAnswers() {
		//System.out.println("Answers size : " + objGalacticConversion.answers);
		System.out.println("OUTPUT : ");
		for(String answer : answers) {
			System.out.println(answer);
		}
	}
	
	// Check if a string is a number. Used for determing if the metal credits are specified as number only
	private Boolean isNumeric(String val) {
		Boolean result = true;
		try {
			Float.parseFloat(val);
		} catch (NumberFormatException e) {
			result = false;
		}
		return result;
	}
	
	// Given a galactic string, converts it into the roman string representation
	private String convertToRoman(String galacticStr) {
		StringBuilder result = new StringBuilder();
		
		String tokens[] = galacticStr.split(" ");
		for(String token : tokens) {
			token = token.trim().toLowerCase();
			if(map_GalacticToRoman.containsKey(token)) {
				result.append(map_GalacticToRoman.get(token));
			} else {
				// For invalid galactic string, append an invalid roman letter!
				result.append("Z");
			}
		}
		return result.toString();
	}
	
	/*public static void main(String[] args) {
		
	}*/
}