import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * @author Yoganand Dharmaraj
 * @description
 * Entry point for the program
 * Please enter the input lines followed by an empty line to view the output
 * 'Assumptions and Design.txt' lists the input formats and the overall purpose of each class
 */

public class Guide {

	public static void main(String[] args) {
		
		GalacticConverter objGalacticConversion = new GalacticConverter();
		
		String inpLine = "";
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			while ((inpLine = bufferRead.readLine()) != null && !inpLine.isEmpty()) {
				objGalacticConversion.processLine(inpLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Display the evaluated answers
		objGalacticConversion.showAnswers();
	}
}
